package main

import (
	"fmt"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello"))
}

func sayHi(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hiiiiiiiii"))
}

func main() {
	http.HandleFunc("/hi", sayHi)
	http.HandleFunc("/hello", sayHello)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("request in")
	})

	http.ListenAndServe(":5555", nil)
}
